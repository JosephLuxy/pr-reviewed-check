var axios = require('axios');

const DOMAIN = 'https://api.bitbucket.org';

const API_VERSION = '2.0';

let key;

module.exports.setKey = (param) => {
    if( 'string' === typeof param){
        key = param;
    }else{
        console.error("Your should input String!")
        throw Error;
    }
}

module.exports.getUserUUID = () => {
    return axios({
        url: `${DOMAIN}/${API_VERSION}/user/permissions/repositories`,
        method: 'GET',
        headers: {
            'Authorization': key,
            'accept': '*/*',
            'charset': 'utf-8',
            'accept-encoding': 'gzip, deflate',
            'user-agent': 'nodejs rest client'
        }
    }).then(res => {
        return res.data;
    }).catch(error => {
        console.log(error);
    });
}

module.exports.getAllPR = async (uuid) => {
    const first_url = `${DOMAIN}/${API_VERSION}/pullrequests/${uuid}`;
    let result = { next: first_url, values: [] };
    while (result.next) {
        const res = await axios({
            url: result.next,
            method: 'GET',
            headers: {
                'Authorization': key,
                'accept': '*/*',
                'charset': 'utf-8',
                'accept-encoding': 'gzip, deflate',
                'user-agent': 'nodejs rest client'
            }
        }).catch(error => {
            console.log(error);
        });
        result.values.push(...res.data.values);
        result.next = res.data.next;
    }
    return result;
}

module.exports.getPRDetail = async (url) => {
   let res = await axios({
        url: url,
            method: 'GET',
            headers: {
                'Authorization': key,
                'accept': '*/*',
                'charset': 'utf-8',
                'accept-encoding': 'gzip, deflate',
                'user-agent': 'nodejs rest client'
            }
    }).catch((error) => {
        console.log(error);
    });
    return checkApprovedCount(res.data.participants||[]);
};

checkApprovedCount = (arr = []) => {
    return arr.filter(element => {
        return element.approved;
    }).length;
};
