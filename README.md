# README #


### What is this repository for? ###

* used to check your all PRs need to be merged or need reviewed.
* Version 1.0.0


### How do I get set up? ###

* [You need set up a bitbucket app password.](https://bitbucket.org/account/settings/app-passwords/)
* you must record your app password{appSercret}.It just display once, and you can`t find it at bitbucket any more.
* Build a string{base64String} of the form username:app_password. Your username{userName} can be found [here](https://bitbucket.org/account/settings/).
```
echo -n username:app_password | base64
```
* Then you can use terminal to check your PRs.
```
npm install
cd re-reviewed-check
node index.js -k {base64String}
or
node index.js -n {userName} -a {appSercret}
or
set enviroment variable {BIT_APP_KEY}
export BIT_APP_KEY = {base64String}
```
* And you can use `-c` to set the `min count of approved`,default is `2`.

---
## THANKS
@Zhang, Nick `Technical support for bitbucket apis`