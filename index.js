const { exit } = require("yargs");
const promise = require("bluebird")
const yargs = require("yargs");
const intergrate = require("../pr-reviewed-check/intergrate");


const param = yargs.alias('k', 'key').alias('n', 'name').alias('a', 'appSercret').alias('c', 'count').argv;
const printList = [];

let auth;
let count = 2;


help = async () => {
    if (!param.key && !param.name && !param.appSercret && !process.env.BIT_APP_KEY) {
        console.log("\n\
        You Need Input Your Key:\n\
        -k [key] Your application basic authorization. \n\
        -n [name] Your account name. \n\
        -a [appSercret] Your application password.");
        exit();
    }
};

init = async () => {
    if (param.key) {
        auth = param.key;
    } else if (param.name && param.appSercret) {
        auth = Buffer.from(param.name + ":" + param.appSercret).toString('base64');
    } else if(process.env.BIT_APP_KEY){
        auth = process.env.BIT_APP_KEY;
    }else{
        console.error("\n\
            You Need Input Your Key:\n \
            -k [key] Your application basic authorization. \n \
            -n [name] Your account name. \n \
            -a [appSercret] Your application password.");
        exit();
    }

    if (param.count) {
        count = param.count;
    }
};

solve = async () => {
    const could = [];
    const cannot = [];
    intergrate.setKey(`basic ${auth}`);
    const uuid = await intergrate.getUserUUID().then((result) => {
        console.log(result.values[0].user.uuid);
        return result.values[0].user.uuid
    });
    const prs = (await intergrate.getAllPR(uuid)).values;
    if (prs.length <= 0) {
        console.log("Congratulations！！！,You have none PR to be merged！");
        return;
    }
    await promise.map(prs,async ele => {
        const cut = await intergrate.getPRDetail(ele.links.self.href)
        if (cut >= 2) {
            could.push(`You can merge this PR , ${cut} reviewers has approved, url =====> ${ele.links.html.href}`);
        } else {
            cannot.push("So sad ~ The RP hasn`t enough approved, url =====> " + ele.links.html.href);
        }
    });
    await solvePrintData(could,cannot);
};

solvePrintData = async (could,cannot) => {
    if(could.length > 0){
        printList.push("You Need Merge ~~~~");
        printList.push(`Could Merge ${could.length}`);
        printList.push(`Cannot Merge ${cannot.length}`);
        printList.push(...could);
        printList.push(...cannot);
    }else{
        printList.push("So Sad T.T No PR can merge ~~~~");
        printList.push(`Cannot Merge ${cannot.length}`);
        printList.push(...cannot);
    }
};

allPrint = async () => {
    printList.forEach(ele => {
        console.log(ele);
    })
};


main = async () => {
    await help();
    await init();
    await solve();
    await allPrint();
};

main();


